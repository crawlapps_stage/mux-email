<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    @include('includes.head')

    @if(config('shopify-app.appbridge_enabled'))
        @include('shopify.app-bridge')
    @endif
    <?php
    header("Content-Security-Policy: frame-ancestors https://".auth()->user()->name."  https://admin.shopify.com");
    ?>
</head>

<body class="antialiased">

<div class="container-fluid">
    <div id="app">
          @yield('content')
    </div>
</div>

@include('includes.footer-script')

</body>

</html>
