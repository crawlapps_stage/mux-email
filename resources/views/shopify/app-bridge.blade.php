    <script src="https://unpkg.com/@shopify/app-bridge@2"></script>
    <script>
        var AppBridge = window['app-bridge'];
        var actions = window['app-bridge'].actions;
        var createApp = AppBridge.default;

        window.shopify_app_bridge = createApp({
            apiKey: '{{config('shopify-app.api_key')}}',
            shopOrigin: '{{ Auth::user()->name }}',
            forceRedirect: true,
        });

    </script>




