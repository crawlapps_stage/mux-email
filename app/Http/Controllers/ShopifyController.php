<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Traits\ShopifyTrait;
use App\Traits\MuxEmailTrait;
use Session;
use Auth;
use DB;
use Carbon\Carbon;
use App\Jobs\BulkContactsJob;

class ShopifyController extends Controller
{

    use ShopifyTrait, MuxEmailTrait;

    public function getCustomerFields(Request $request)
    {

        $errors = [];

        try {

            $attributes = $this->getShopifyCustomerFields();

            return ['success' => true, 'errors' => $errors, 'attributes' => $attributes];

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

    public function getCustomersCount(Request $request)
    {

        $errors = [];

        try {

            $shop = Auth::user();

            $customers_count = 0;

            $customers = $this->getShopifyCustomers($shop);

            if (count($customers) > 0) {
                $customers_count = count($customers);
            }

            return ['success' => true, 'errors' => $errors, 'customers_count' => $customers_count];

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


    public function getBulkContacts(Request $request)
    {

        $errors = [];

        try {
            $shop = Auth::user();

            $last_synced_date = null;

            $created_at = DB::table('bulk_contacts')->where('user_id',
                $shop->id)->select('created_at')->orderBy('created_at', "DESC")->value('created_at');
            if ($created_at) {
                $last_synced_date = Carbon::parse($created_at)->isoFormat('Do MMM Y, h:mm:ss a');
            }

            return ['success' => true, 'errors' => $errors, 'last_synced_date' => $last_synced_date];


        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


    public function BulkUploadSyncContacts(Request $request)
    {

        $errors = [];

        try {
            $shop = Auth::user();

            $input = $request->all();

            BulkContactsJob::dispatch($shop->id, $input);

            $jobmsg = 'Some Contact are being pushed in the background.';

            return Response::json(['success' => true, 'message' => $jobmsg], 200);


        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


}
