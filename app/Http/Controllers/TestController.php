<?php

namespace App\Http\Controllers;

use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use App\Traits\MuxEmailTrait;
use App\Jobs\CustomersCreateJob;
use App\Jobs\CustomersDeleteJob;
use Auth;
use Response;
class TestController extends Controller
{

    use MuxEmailTrait;
    use ShopifyTrait;

    public $name;
    public function __construct(){
        $this->name = "Test MODE";
    }


    public function index(){

        $shop = Auth::user();

        $input = '{
                  "id": 706405506930370084,
                  "email": "bob@biller.com",
                  "accepts_marketing": true,
                  "created_at": null,
                  "updated_at": null,
                  "first_name": "Bob",
                  "last_name": "Biller",
                  "orders_count": 0,
                  "state": "disabled",
                  "total_spent": "0.00",
                  "last_order_id": null,
                  "note": "This customer loves ice cream",
                  "verified_email": true,
                  "multipass_identifier": null,
                  "tax_exempt": false,
                  "phone": null,
                  "tags": "",
                  "last_order_name": null,
                  "currency": "USD",
                  "addresses": [
                
                            ],
                  "accepts_marketing_updated_at": null,
                  "marketing_opt_in_level": null,
                  "admin_graphql_api_id": "gid:\/\/shopify\/Customer\/706405506930370084"
                }';

        $input = json_encode(json_decode($input));

        CustomersCreateJob::dispatch($shop->name, $input);

    }

    //create webhook
    public function RegisterWebhooksTest(){


        $shop = Auth::user();

        $existWebhook = [];

        $webhooksListRes = webhooksList($shop->id);

        if(!$webhooksListRes['errors']){
            $existWebhook = (array) $webhooksListRes['body']['webhooks']['container'];
        }

        $topics = array_column($existWebhook, 'topic');


       $webhook_input = [];
       $webhook_list = [
                [
                   'topic' =>  'customers/create',
                   'address' => env('APP_URL').'/webhook/customers-create',
                   "format" => "json",
                ],
                [
                   'topic' =>  'customers/update',
                   'address' => env('APP_URL').'/webhook/customers-update',
                   "format" => "json",
                ]
           ];

            foreach($webhook_list as $webhook){

                if(!in_array($webhook['topic'],$topics)){
                    logger("webhook exist :: ".$webhook['topic']);
                    $webhook_input[] = $webhook;
                }
            }



        if(count($webhook_input)>0) {
            foreach($webhook_input as $webhook) {
                $param = $webhook;
                $res = $this->createWebhooks($shop, $param);
            }
        }


        return  Response::json(['success' => true, 'message' => 'Webhooks List.'], 200);

    }


    public function RegisteredwebhooksList(){

        $shop = Auth::user();
        $res = webhooksList($shop->id);

        dd($res);
        return  Response::json(['success' => true,"data"=>$res, 'message' => 'Webhooks List.'], 200);

    }

    public function TestAPIData(){
        return [
            "name"=> $this->name
        ];
    }

    public function connectMuxemailTest(){

        $res = $this->connectMuxEmailAccountTEST();

        dd($res);

    }


    public function deleteMuxEmailContact(){


        $shop = Auth::user();
        $param = [
                "email" => "amit@muxemail.com"
        ];

        $res = $this->deleteContact($shop,$param);

        dd($res);

    }


    public function deleteCustomerWebhook(){


        $shop = Auth::user();

        $input = [
            "id" => 3836725133472
        ];

       $res =  CustomersDeleteJob::dispatch($shop->name, $input);

        dd($res);

    }



}
