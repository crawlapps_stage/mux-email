<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Models\MatchAttributesFields;
use App\Traits\ShopifyTrait;
use App\Traits\MuxEmailTrait;
class AppController extends Controller
{

    use ShopifyTrait,MuxEmailTrait;

    public function AddAttributesFields(Request $request){

        $errors = [];

        try {

            $input = $request->all();

            $attributes = $input['attributes'];

            $shop = Auth::user();

            DB::table('match_attributes_fields')->where('user_id',$shop->id)->delete();

            $insert = [];

            foreach($attributes as $row)
            {

                $draw = [
                    'user_id' => $shop->id,
                    'shopify_field_key' => $row['field1']['fieldKey'],
                    'muxemail_field_key' =>$row['field2']['fieldKey'],
                    'created_at'  => date("Y-m-d H:i:s"),
                    'updated_at'  => date("Y-m-d H:i:s")
                ];

                DB::table('match_attributes_fields')->updateOrInsert(['user_id' => $shop->id,'muxemail_field_key' => $row['field2']['fieldKey']],$draw);
            }

            return  Response::json(['success' => true,'message' => 'Fields Saved successfully.'], 200);


        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


    public function GetAssignTags(Request $request){

        $errors = [];

        try {

            $shop = Auth::user();

            $tags =  DB::table('assign_tags')->select('name')->where('user_id',$shop->id)->get();

            return ['success' => true, 'errors' => $errors, 'tags' => $tags];

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

    public function SaveTags(Request $request){

        $errors = [];

        try {

            $input = $request->all();

            $tags = $input['tags'];

            $shop = Auth::user();

            DB::table('assign_tags')->where('user_id',$shop->id)->delete();

            $insert = [];

            foreach($tags as $row)
            {

                $draw = [
                    'user_id' => $shop->id,
                    'name' => $row['name'],
                    'created_at'  => date("Y-m-d H:i:s"),
                    'updated_at'  => date("Y-m-d H:i:s")
                ];

                DB::table('assign_tags')->Insert($draw);
            }

            return  Response::json(['success' => true,'message' => 'Tags Saved successfully.'], 200);


        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

    public function GetMatchAttributesFields(Request $request){

        $errors = [];

        try {

            $shop = Auth::user();

            $match_fields = MatchAttributesFields::where('user_id',$shop->id)->get()->toArray();
            if(count($match_fields)>0){

                $attributes = [];
                $mux_attributes = $this->getAllSubscriberFieldsData($shop);

                foreach($mux_attributes as $key=>$val){
                    if(!isset($val['fieldLabel'])){
                        $attributes[$key]['fieldLabel'] = $val['fieldKey'];
                    }
                    else{
                        $attributes[$key]['fieldLabel'] = $val['fieldLabel'];
                    }
                    $attributes[$key]['fieldKey'] = $val['fieldKey'];
                }

                $shopify_attributes = $this->getShopifyCustomerFields();

                $match_attributes = [];

                for($f=0; $f<count($match_fields); $f++){

                    $fields = [];

                    $customer_key = $match_fields[$f]['shopify_field_key'];
                    $mux_key = $match_fields[$f]['muxemail_field_key'];

                       foreach($shopify_attributes as $att){

                           if($att->fieldKey==$customer_key){
                               $fields['field1'] = $att;
                           }

                       }

                    foreach($attributes as $att){

                        if($att['fieldKey']==$mux_key){
                            $fields['field2'] = $att;
                        }
                    }

                    $match_attributes[] = $fields;
                }


            }else{
                $match_attributes =
                    [
                        [
                            "field1" => [
                                "fieldKey" => "email" ,
                                "fieldLabel" => "Email",
                             ],
                            "field2" => [
                                 "fieldKey" => "email",
                                 "fieldLabel" => "Email",
                            ],
                        ],
                        [
                            "field1" => [
                                "fieldKey" => "first_name",
                                "fieldLabel" => "First name",
                            ],
                            "field2" => [
                                "fieldKey" => "fname",
                                "fieldLabel" => "First name",
                            ],
                        ],
                        [
                            "field1" => [
                                "fieldKey" => "last_name",
                                "fieldLabel" => "Last name",
                            ],
                            "field2" => [
                                "fieldKey" => "lname",
                                "fieldLabel" => "Last name",
                            ],
                        ]

                       ];


            }


            return ['success' => true, 'errors' => $errors, 'match_attributes' => $match_attributes];

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

}
