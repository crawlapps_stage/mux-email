<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Traits\MuxEmailTrait;
use Session;
use Auth;
use App\Models\MuxEmailCredential;

class MuxEmailController extends Controller
{

    use MuxEmailTrait;

    public function connectMuxEmail(Request $request)
    {

        try {
            $shop = Auth::user();
            $input = $request->all();

            logger("muxCredential");
            logger(json_encode($input));

            $data["status"] = "failed";

            $muxCredential = [
                "api_key" => $input['api_key'],
                "workspace_id" => $input['workspace_id']
            ];

            $res = $this->connectMuxEmailAccount($muxCredential);

            logger("mux-email-connetc-res");
            logger(json_encode($res));

            if ($res) {
                if (isset($res['status']) && $res['status'] == "Success") {
                    $data["status"] = "Success";

                    $user = MuxEmailCredential::firstOrNew(['user_id' => $shop->id]);
                    $user->user_id = $shop->id;
                    $user->api_key = $input['api_key'];
                    $user->workspace_id = $input['workspace_id'];
                    $user->save();

                    return Response::json([
                        'success' => true, "mux_is_connect" => true, 'message' => 'Mux-Email Connected successfully.'
                    ], 200);

                } else {
                    if (isset($res['error'])) {

                        return Response::json([
                            'success' => false, "mux_is_connect" => false, 'message' => $res['error']
                        ], 200);
                    } else {
                        return ['success' => false, 'message' => "Something went wrong. please try again"];
                    }
                }
            }

            // return  Response::json(['success' => true,"data" => $data,'message' => 'Mux-Email Connected successfully.'], 200);

        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }

    }

    public function CheckCredential(Request $request)
    {

        try {
            $shop = Auth::user();

            $data = muxMailCredentialData($shop);

            if (isset($data['api_key'])) {
                return Response::json([
                    'success' => true, "mux_is_connect" => true, 'message' => 'Mux-Email Connected successfully.'
                ], 200);
            } else {
                return Response::json(['success' => true, "mux_is_connect" => false], 200);
            }
            // return  Response::json(['success' => true,"data" => $data,'message' => 'Mux-Email Connected successfully.'], 200);

        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }

    }


    public function getAttributesFields(Request $request)
    {

        $errors = [];

        try {

            $shop = Auth::user();

            $attributes = [];
            $mux_attributes = $this->getAllSubscriberFieldsData($shop);
            if (isset($mux_attributes['error'])) {
                return Response::json(['success' => false,"attributes" => [], 'message' => $mux_attributes['error']], 200);
            }else {
                foreach ($mux_attributes as $key => $val) {
                    if (!isset($val['fieldLabel'])) {
                        $attributes[$key]['fieldLabel'] = $val['fieldKey'];
                    } else {
                        $attributes[$key]['fieldLabel'] = $val['fieldLabel'];
                    }
                    $attributes[$key]['fieldKey'] = $val['fieldKey'];
                }

                return ['success' => true, 'errors' => $errors, 'attributes' => $attributes];
            }

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


    public function getAllTags(Request $request)
    {

        $errors = [];

        try {

            $shop = Auth::user();

            $tags = $this->getAllTagsData($shop);
            if (isset($tags['error'])) {
                return Response::json(['success' => false, 'message' => $tags['error']], 200);
            }else {
                return ['success' => true, 'errors' => $errors, 'tags' => $tags];
            }

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }

    public function getAllWorkspaces(Request $request)
    {

        $errors = [];

        try {

            $shop = Auth::user();

            $input = $request->all();

            $api_key =  $input['api_key'];
            $exist_api_key = MuxEmailCredential::where('api_key',$api_key)->first();

            if(!$exist_api_key) {

                $workspaces = $this->getAllWorkspacesData($shop, $input);

                if (isset($workspaces['error'])) {

                    return Response::json(['success' => false, 'message' => $workspaces['error']], 200);
                } else {
                    return ['success' => true, 'errors' => $errors, 'workspaces' => $workspaces];
                }
            }else{
                return Response::json(['success' => false, 'message' => "This key already been used with another store" ], 200);
            }


        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }
    }


}
