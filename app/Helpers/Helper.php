<?php

use App\Models\ShopifyShop;
use Illuminate\Support\Facades\Auth;
use App\Models\MuxEmailCredential;
use App\Models\User;
use DB as DBS;


if(!function_exists('muxMailCredentialData')){

    function muxMailCredentialData($shop){
        try{

            $data = MuxEmailCredential::select('api_key','workspace_id')->where('user_id',$shop->id)->first();
            return $data;
        }
        catch(\Exception $e){
            logger('=========== ERROR:: Mux Mail Credential Data===========');
            logger(json_encode($e));
        }
    }

}

if (!function_exists('webhooksList')) {
    function webhooksList($shop_id)
    {
        try {

            logger('================= START::webhooks List =================');

            $shop = User::where('id', $shop_id)->first();

            $webhookReq = $shop->api()->rest('GET', '/admin/api/'.env('SHOPIFY_API_VERSION').'/webhooks.json');

            logger("===============webhooks :: Result=================");
            logger(json_encode($webhookReq));
            logger('================= END::  webhooks List =================');

            return $webhookReq;

        } catch (\Exception $e) {
            logger('================= ERROR::  webhooks List =================');
            logger($e->getMessage());
            return true;
        }
    }
}

if (!function_exists('getShopDataFromAPI')) {
    function getShopDataFromAPI($user, $fields)
    {
        logger('=========== START:: getShopDataFromAPI ===========');
        try {
            $shop = Auth::user();
            $parameter['fields'] = $fields;

            $shop_result = $shop->api()->rest('GET', 'admin/api/'.env('SHOPIFY_API_VERSION').'/shop.json', $parameter);

            if (!$shop_result->errors) {
                return $shop_result->body->shop;
            }

        } catch (\Exception $e) {
            logger('=========== ERROR:: getShopDataFromAPI ===========');
            logger(json_encode($e));
        }
    }
}


if (!function_exists('getShopData')) {
    function getShopData($user_id)
    {
        try {

            logger('=========== START:: getShopData ===========');

            logger("Shop ID :: ". $user_id);

            $data = ShopifyShop::select('shop_id','name','email','domain','currency','country_code')->where('user_id',$user_id)->first();

            return $data;

        } catch (\Exception $e) {
            logger('=========== ERROR:: getShopData ===========');
            // logger(json_encode($e));
        }
    }
}
