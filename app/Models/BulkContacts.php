<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BulkContacts extends Model
{
    use HasFactory;

    protected $table = 'bulk_contacts';

    protected $fillable = [
        'shopify_id',
        'contacts',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'contacts' => 'array',
    ];

    public $timestamps = true;


}
