<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchAttributesFields extends Model
{
    use HasFactory;


    protected $fillable = [
        'shopify_field_key',
        'muxemail_field_key',
        'created_at',
        'updated_at'
    ];


    public $timestamps = true;

}
