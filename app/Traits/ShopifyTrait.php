<?php

namespace App\Traits;
use DB;


trait ShopifyTrait{


    public function getShopifyCustomerFields()
    {

        try {

            $response = '[
                    {
                        "fieldLabel" : "Email",
                        "fieldKey": "email"
                    },
                    {
                        "fieldLabel": "Phone",
                        "fieldKey": "phone"
                    },
                    {
                        "fieldLabel": "First name",
                        "fieldKey": "first_name"
                    },
                    {
                        "fieldLabel": "Last name",
                        "fieldKey": "last_name"
                    },
                    {
                        "fieldLabel": "Full name",
                        "fieldKey": "name"
                    },
                    {
                        "fieldLabel": "Address line 1",
                        "fieldKey": "address1"
                    },
                    {
                        "fieldLabel": "Address line 2",
                        "fieldKey": "address2"
                    },
                    {
                        "fieldLabel": "City",
                        "fieldKey": "city"
                    },
                    {
                        "fieldLabel": "State (Province)",
                        "fieldKey": "province"
                    },
                    {
                        "fieldLabel": "Province Code",
                        "fieldKey": "province_code"
                    },
                    {
                        "fieldLabel": "Country",
                        "fieldKey": "country"
                    },
                    {
                        "fieldLabel": "Country Code",
                        "fieldKey": "country_code"
                    },
                    {
                        "fieldLabel": "Zip",
                        "fieldKey": "zip"
                    },
                    {
                        "fieldLabel": "Currency",
                        "fieldKey": "currency"
                    }

                ]';


        $data = json_decode($response);

            //logger("Cus-Fields-DATA");
           // logger(json_encode($data));

           return $data;


        } catch (\Exception $e) {

            logger(json_encode($e));
        }
    }

    public function getShopifyCustomers($shop)
    {

        try {

            $errors = [];

            $customers = [];

            $apiRequest = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/customers.json', ['limit' => 250]);

            if (isset($apiRequest['body']['customers'])) {
                $customers_data = $apiRequest['body']['customers'];


                foreach($customers_data as $customer){

                   // logger("accepts_marketing ::".$customer['accepts_marketing']);

                    if($customer['accepts_marketing'] === true && $customer['email']!==null) {

                        $exist_sync_customer = DB::table('bulk_contacts')->where([
                            'user_id' => $shop->id, 'shopify_id' => $customer['id']
                        ])->get();

                        if (count($exist_sync_customer) <= 0) {

                            $customers[] = $customer;

                            logger("true");
                        }

                    }

                }

            }

           //  logger("customers :: ".json_encode($customers));

            return $customers;


        } catch (\Exception $e) {
            logger('=========== ERROR:: customers ===========');
            logger(json_encode($e));
        }
    }


    public function createWebhooks($shop,$webhooks_input)
    {

        try {

            logger('=========== START:: create webhooks ===========');

            $errors = [];

            $apiRequest = $shop->api()->rest('POST', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/webhooks.json', ['webhook' => $webhooks_input]);

            logger("webhooks Create :: ".json_encode($apiRequest));

            return true;


        } catch (\Exception $e) {
            logger('=========== ERROR::create  webhooks ===========');
            logger(json_encode($e));
        }
    }


    public function RegisterWebhooks($shop){

        logger("======START :: webhook Resgistor========");

        $existWebhook = [];

        $webhooksListRes = webhooksList($shop->id);

        if(!$webhooksListRes['errors']){
            $existWebhook = (array) $webhooksListRes['body']['webhooks']['container'];
        }

        $topics = array_column($existWebhook, 'topic');

        logger("===exist webhook topic====");

        logger($topics);


        $webhook_input = [];
        $webhook_list = [
            [
                'topic' =>  'customers/create',
                'address' => env('APP_URL').'/webhook/customers-create',
                "format" => "json",
            ],
            [
                'topic' =>  'customers/update',
                'address' => env('APP_URL').'/webhook/customers-update',
                "format" => "json",
            ],
            [
                'topic' =>  'customers/delete',
                'address' => env('APP_URL').'/webhook/customers-delete',
                "format" => "json",
                "fields" => [
                       "id",
                       "email"
                 ],
            ]
        ];

        foreach($webhook_list as $webhook){

            if(!in_array($webhook['topic'],$topics)){
                logger("webhook new :: ".$webhook['topic']);
                $webhook_input[] = $webhook;
            }
        }


        logger("=======Webhook Input=========");
        logger(json_encode($webhook_input));

        if(count($webhook_input)>0) {
            foreach($webhook_input as $webhook) {
                $param = $webhook;
                $res = $this->createWebhooks($shop, $param);
            }
        }

        return true;

        logger("======END :: webhook Resgistor========");
      //  return  Response::json(['success' => true,"data"=>$res, 'message' => 'Webhooks List.'], 200);

    }


}
