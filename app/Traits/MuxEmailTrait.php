<?php

namespace App\Traits;
use DB;
use Illuminate\Support\Facades\Session;


trait MuxEmailTrait{

    public function connectMuxEmailAccount($muxCredential){

        $workspace_id = $muxCredential['workspace_id'];
        $api_key =  $muxCredential['api_key'];

        $api_url = config("const.muxemail_api_url");

        $apiAuthURL = $api_url."/v1/authentiticationWithTokens";
//        https://apistest.muxemail.com/v1/authentiticationWithTokens
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiAuthURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'apikey:'.$api_key,
                'workspaceid:'.$workspace_id
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($response, true);

        return $data;

    }


    public function getAllSubscriberFieldsData($shop){

        $endpoint = "v1/getAllSubscriberFields";
        $param = [];
        $method = "GET";
        $data = $this->RequestMuxEmailAPI($shop,$endpoint,$method,$param);

        return $data;

    }


    public function getAllTagsData($shop){

        $endpoint = "v1/getAllTags";
        $param = [];
        $method = "GET";
        $data = $this->RequestMuxEmailAPI($shop,$endpoint,$method,$param);

        return $data;

    }


    public function addOrUpdateContactBulk($shop,$param){

        $endpoint = "v1/addOrUpdateContact";
        $method = "POST";
        $data = $this->RequestMuxEmailAPI($shop,$endpoint,$method,$param);

        logger("=====addOrUpdateContact Customer RES======");
        logger(json_encode($data));

        return $data;

    }


    public function unSubscribeContact($shop,$param){

        $endpoint = "v1/unsubscribeContact/".$param;
        $method = "PUT";
        $data = $this->RequestMuxEmailAPI($shop,$endpoint,$method,[]);

        logger("=====Unsubscribe Customer RES======");
        logger(json_encode($data));

        return $data;

    }

    public function deleteContact($shop,$param){

        logger("Deleted Contact...");

        $endpoint = "v1/deleteContact";
        $method = "DELETE";
        $data = $this->RequestMuxEmailAPI($shop,$endpoint,$method,$param);

        logger("=====Delete Customer RES======");
        logger(json_encode($data));

        return $data;

    }


    public function getAllWorkspacesData($shop,$data){

        $api_key =  $data['api_key'];

        logger(json_encode($data,true));

        $api_url = config("const.muxemail_api_url");

        $apiAuthURL = $api_url."/v1/getAllWorkspaces";
//        https://apistest.muxemail.com/v1/getAllSubscriberFields
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiAuthURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'apikey:'.$api_key,
                'Content-Type:application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($response, true);

        // logger(json_encode($data));

        return $data;

    }


    public function RequestMuxEmailAPI($shop,$endpoint,$method="GET",$param=[]){

        $api_url = config("const.muxemail_api_url");
        $apiAuthURL = $api_url."/".$endpoint;

        logger("API URL =============  ".$apiAuthURL);

        $muxCredential = muxMailCredentialData($shop);
        $workspace_id = $muxCredential['workspace_id'];
        $api_key =  $muxCredential['api_key'];

        $data = [];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $apiAuthURL,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => json_encode($param,true),
                CURLOPT_HTTPHEADER => array(
                    'apikey:'.$api_key,
                    'workspaceid:'.$workspace_id,
                    'Content-Type:application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $data = json_decode($response, true);

            if(isset($data['error'])){

             if($data['error']=="MuxToken isn't valid anymore"){

                       DB::table("mux_email_credentials")->where('user_id',$shop->id)->delete();

             }

            }

        return $data;


    }



}
