<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\MatchAttributesFields;

use DB;
use App\Traits\ShopifyTrait;
use App\Traits\MuxEmailTrait;

class BulkContactsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyTrait, MuxEmailTrait;

    private $shop_id = '';
    private $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id, $data)
    {
        $this->shop_id = $shop_id;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger("===============START :: JOB :: Add Bulk Contact Sync ===============");
        $shop = User::where('id', $this->shop_id)->first();
        logger(json_encode($this->data));

        try {

            $input = $this->data;

            $tags_input = $input['tags'];

            $tags = array_column($tags_input, 'name');

            $customers = $this->getShopifyCustomers($shop);

            $addresses_key = [
                "address1", "address2", "city", "province", "country", "zip", "name", "province_code",
                "country_code"
            ];

            if (count($customers) > 0) {
                for ($c = 0; $c < count($customers); $c++) {

                    logger("=========customer=======".$c);
                    logger(json_encode($customers[$c]));

                    if ($customers[$c]['email'] !== null && $customers[$c]['accepts_marketing'] === true ) {

                        $exist_sync_customer = DB::table('bulk_contacts')->where([
                            'user_id' => $shop->id, 'shopify_id' => $customers[$c]['id']
                        ])->get();

                        if (count($exist_sync_customer) <= 0) {
                            $match_fields = MatchAttributesFields::where('user_id', $this->shop_id)->get()->toArray();

                            logger("==========match_fields========");
                            logger(json_encode($match_fields));

                            if (count($match_fields) > 0) {

                                for ($f = 0; $f < count($match_fields); $f++) {

                                    $customer_key = $match_fields[$f]['shopify_field_key'];
                           
                                    if (in_array($customer_key, $addresses_key)) {

                                          logger("key".$customer_key);

                                       if((isset($customers[$c]['addresses']) && count($customers[$c]['addresses'])) || (isset($customers[$c]['default_address']) &&  count($customers[$c]['default_address']))){
                                        $customer_val = (@$customers[$c]['addresses'][0]->$customer_key) ? $customers[$c]['addresses'][0]->$customer_key : (@$customers[$c]['default_address']->$customer_key ? $customers[$c]['default_address']->$customer_key : '');

                                        $row[$match_fields[$f]['muxemail_field_key']] = $customer_val;
                                    }

                                    } else {
                                        $row[$match_fields[$f]['muxemail_field_key']] = (@$customers[$c][$customer_key]) ? $customers[$c][$customer_key] : '';

                                    }

                                }

                            } else {

                                $row = [
                                    "email" => $customers[$c]['email'],
                                    "fname" => $customers[$c]['first_name'],
                                    "mname" => "",
                                    "lname" => $customers[$c]['last_name'],
                                    "phone" => $customers[$c]['phone'],
                                    "gender" => "",
                                    "dob" => ""                                   
                                ];

                            if((isset($customers[$c]['addresses']) && count($customers[$c]['addresses'])) || (isset($customers[$c]['default_address']) &&  count($customers[$c]['default_address']))){

                                        $row["addressLine1"] = (@$customers[$c]['addresses'][0]->address1) ? $customers[$c]['addresses'][0]->address1 : (@$customers[$c]['default_address']->address1 ? $customers[$c]['default_address']->address1 : '');

                                        $row["addressLine2"] =  (@$customers[$c]['addresses'][0]->address2) ? $customers[$c]['addresses'][0]->address2 : (@$customers[$c]['default_address']->address2 ? $customers[$c]['default_address']->address2 : '');

                                        $row["city"] =  (@$customers[$c]['addresses'][0]->city) ? $customers[$c]['addresses'][0]->city : (@$customers[$c]['default_address']->city ? $customers[$c]['default_address']->city : '');

                                         $row["state"] =  (@$customers[$c]['addresses'][0]->province) ? $customers[$c]['addresses'][0]->province : (@$customers[$c]['default_address']->province ? $customers[$c]['default_address']->province : '');

                                        $row["postalCode"] =  (@$customers[$c]['addresses'][0]->zip) ? $customers[$c]['addresses'][0]->zip : (@$customers[$c]['default_address']->zip ? $customers[$c]['default_address']->zip : '');

                                           $row["country"] = (@$customers[$c]['addresses'][0]->country) ? $customers[$c]['addresses'][0]->country : (@$customers[$c]['default_address']->country ? $customers[$c]['default_address']->country : '');

                               }else{
                                     $row["addressLine1"] = "";
                                     $row["addressLine2"] = "";
                                     $row["city"] = "";
                                     $row["state"] = "";
                                     $row["postalCode"] = "";
                                     $row["country"] = "";
                               }

                            }

                            $row["source"] = "Shopify";
                           
                           
                         logger("addresses---");
                         logger(json_encode($row));

                           $shope_data = getShopData($this->shop_id);

                            if($shope_data){
                                $row["sourceId"] = $shope_data->shop_id;
                            }else{
                                $shope_data = getShopDataFromAPI($shop, 'id, email, name');
                                $row["sourceId"] = $shope_data->id;
                            }

                            $row["status"] = "Subscribed";
                            $row["custom"] = [];
                            $row["tags"] = $tags;

                            logger("===========================ROW==================================");
                            logger(json_encode($row));
                            logger("=============================================================");


                            $bulkRes = $this->addOrUpdateContactBulk($shop, $row);

                            logger("Bulk Contact res");
                            logger(json_encode($bulkRes));

                            if ($bulkRes['status'] == 200) {

                                logger("res-status".$bulkRes['status']);

                                $draw = [
                                    'user_id' => $shop->id,
                                    'shopify_id' => $customers[$c]['id'],
                                    'contacts' => json_encode($row, true),
                                    'created_at' => date("Y-m-d H:i:s"),
                                    'updated_at' => date("Y-m-d H:i:s")
                                ];

                                DB::table('bulk_contacts')->updateOrInsert([
                                    'user_id' => $shop->id, 'shopify_id' => $customers[$c]['id']
                                ], $draw);

                            }
                        }
                    }
                }

                $this->RegisterWebhooks($shop);

            }


            logger("===============END :: JOB :: Add Bulk Contact Sync ===============");

            return true;

        } catch (\Exception $e) {
            logger("===============ERROR :: JOB :: Add Bulk Contact Sync ===============");
            logger(json_encode($e->getMessage()));

        }
    }
}
