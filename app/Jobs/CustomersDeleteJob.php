<?php namespace App\Jobs;


use App\Models\User;
use App\Traits\MuxEmailTrait;
use App\Traits\ShopifyTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use DB;
class CustomersDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyTrait,MuxEmailTrait;
    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger("===============START :: JOB :: Customers Delete ===============");
            $shop = User::where('name', $this->shopDomain)->first();

            logger("=====Customer-Delete/Webhook Data=====");
            logger(json_encode($this->data));
              
              $data = (array) $this->data;

            $customer_id = $data['id'];

            logger("customer_id ::".$customer_id);

            $exist_sync_customer = DB::table('bulk_contacts')->select('contacts->email as email')->where([
                'user_id' => $shop->id, 'shopify_id' => $customer_id
            ])->first();


            logger("exist_sync_customer");

            if($exist_sync_customer && isset($exist_sync_customer->email)) {

                logger($exist_sync_customer->email);

                $param = $exist_sync_customer;

                $res = $this->deleteContact($shop, $param);

                if(isset($res['status']) && $res['status']=="Success"){
                 DB::table('bulk_contacts')->where([
                        'user_id' => $shop->id, 'shopify_id' => $customer_id
                    ])->delete();
                }

            }
            logger("===============END :: JOB :: Customers Delete ===============");
            return response()->json(['data' => 'success'], 200);

        }catch( \Exception $e ){
            logger("===============ERROR :: JOB :: Customers Delete ===============");
            logger(json_encode($e->getMessage()));
        }
    }
}
