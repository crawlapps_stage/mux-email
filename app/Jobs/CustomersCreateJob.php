<?php namespace App\Jobs;

use App\Models\MatchAttributesFields;
use App\Models\User;
use App\Traits\MuxEmailTrait;
use App\Traits\ShopifyTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use App\Jobs\BulkContactsJob;
use DB;
use App\Events\CheckCustomers;

class CustomersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyTrait,MuxEmailTrait;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger("===============START :: JOB :: Customers Create ===============");
            $user = User::where('name', $this->shopDomain)->first();
            event(new CheckCustomers($user->id, $this->data));


        return response()->json(['data' => 'success'], 200);

        }catch( \Exception $e ){
            logger("===============ERROR :: JOB :: Customers Create ===============");
            logger(json_encode($e->getMessage()));
        }
    }
}
