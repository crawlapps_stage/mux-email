<?php

namespace App\Listeners;

use App\Events\CheckCustomers;
use App\Models\MatchAttributesFields;
use App\Models\User;
use App\Traits\ShopifyTrait;
use App\Traits\MuxEmailTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use DB;
class CustomersCreateUpdate
{

    use ShopifyTrait,MuxEmailTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CheckCustomers  $event
     * @return void
     */
    public function handle(CheckCustomers $event)
    {
        try{
            logger("===============START :: Listener :: Customers Create/Update ===============");

            $ids = $event->ids;
            $user_id = $ids['user_id'];
            $data = $ids['data'];

            $shop = User::where('id', $user_id)->first();

            $customer = (array) $data;

            logger("customer webhook data");
            logger(json_encode($customer));

            logger($customer['email']);

            $tags_data =  DB::table('assign_tags')->select('name')->where('user_id',$shop->id)->get()->pluck('name');

            $addresses_key = ["address1","address2","city","province","zip","name","province_code","country_code"];

            if ($customer['email'] !== null) {

//                $exist_sync_customer = DB::table('bulk_contacts')->where([
//                    'user_id' => $shop->id, 'shopify_id' => $customer['id']
//                ])->get();

               // if (count($exist_sync_customer) <= 0) {
                    $match_fields = MatchAttributesFields::where('user_id', $shop->id)->get()->toArray();

                    logger("==========match_fields========");
                    logger(json_encode($match_fields));

                    if (count($match_fields) > 0) {

                        logger("if");
                       
                        for ($f = 0; $f < count($match_fields); $f++) {

                            $customer_key = $match_fields[$f]['shopify_field_key'];

                            if (in_array($customer_key, $addresses_key)) {

 if((isset($customers['addresses']) && count($customers['addresses'])) || (isset($customers['default_address']) &&  count($customers['default_address']))){
                                $customer_val = (@$customer['addresses'][0]->$customer_key) ? $customer['addresses'][0]->$customer_key : (@$customer['default_address']->$customer_key ? $customer['default_address']->$customer_key : '');


                                $row[$match_fields[$f]['muxemail_field_key']] = $customer_val;
                            }

                            } else {
                                $row[$match_fields[$f]['muxemail_field_key']] = (@$customer[$customer_key]) ? $customer[$customer_key] : '';
                            }

                        }


                    } else {

                        logger("else");
                      
                        $row = [
                            "email" => $customer['email'],
                            "fname" => $customer['first_name'],
                            "mname" => "",
                            "lname" => $customer['last_name'],
                            "phone" => $customer['phone'],
                            "gender" => "",
                            "dob" => "",                           

                        ];

                          if((isset($customers['addresses']) && count($customers['addresses'])) || (isset($customers['default_address']) &&  count($customers['default_address']))){

                                        $row["addressLine1"] = (@$customers['addresses'][0]->address1) ? $customers['addresses'][0]->address1 : (@$customers['default_address']->address1 ? $customers['default_address']->address1 : '');

                                        $row["addressLine2"] =  (@$customers['addresses'][0]->address2) ? $customers['addresses'][0]->address2 : (@$customers['default_address']->address2 ? $customers['default_address']->address2 : '');

                                        $row["city"] =  (@$customers['addresses'][0]->city) ? $customers['addresses'][0]->city : (@$customers['default_address']->city ? $customers['default_address']->city : '');

                                         $row["state"] =  (@$customers['addresses'][0]->province) ? $customers['addresses'][0]->province : (@$customers['default_address']->province ? $customers['default_address']->province : '');

                                        $row["postalCode"] =  (@$customers['addresses'][0]->zip) ? $customers['addresses'][0]->zip : (@$customers['default_address']->zip ? $customers['default_address']->zip : '');

                                           $row["country"] = (@$customers['addresses'][0]->country) ? $customers['addresses'][0]->country : (@$customers['default_address']->country ? $customers['default_address']->country : '');

                               }else{
                                     $row["addressLine1"] = "";
                                     $row["addressLine2"] = "";
                                     $row["city"] = "";
                                     $row["state"] = "";
                                     $row["postalCode"] = "";
                                     $row["country"] = "";
                               }

                    }

                    $row["source"] = "Shopify";
                  
                    $shope_data = getShopData($shop->id);

                    if($shope_data){
                        $row["sourceId"] = $shope_data->shop_id;
                    }else{
                        $shope_data = getShopDataFromAPI($shop, 'id, email, name');
                        $row["sourceId"] = $shope_data->id;
                    }

                    $row["custom"] = [];
                    $row["tags"] = $tags_data;

                    if($customer['accepts_marketing'] === true){
                        $row["status"] = "Subscribed";

                        $bulkRes = $this->addOrUpdateContactBulk($shop, $row);
                    }else{
                        $row["status"] = "Unsubscribed";

                        $customerEmail = $customer['email'];

                        $bulkRes = $this->unSubscribeContact($shop, $customerEmail);
                    }


                    logger("===========================ROW==================================");
                    logger(json_encode($row));
                    logger("=============================================================");


                    logger("Bulk Contact res");
                    logger(json_encode($bulkRes));

                    if ($bulkRes['status'] == 200) {

                        logger("res-status".$bulkRes['status']);

                        $draw = [
                            'user_id' => $shop->id,
                            'shopify_id' => $customer['id'],
                            'contacts' => json_encode($row, true),
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")
                        ];

                        DB::table('bulk_contacts')->updateOrInsert([
                            'user_id' => $shop->id, 'shopify_id' => $customer['id']
                        ], $draw);

                    }
               // }
            }


            return response()->json(['data' => 'success'], 200);

        }catch( \Exception $e ){
            logger("===============ERROR :: Listener :: Customers Create/Update ===============");
            logger(json_encode($e->getMessage()));
        }
    }
}
