<?php


$MUXEMAIL_API_URL = env('MUXEMAIL_API_URL', 'https://apistest.muxemail.com');



return [

    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    */

    'muxemail_api_url'   => $MUXEMAIL_API_URL,


];
