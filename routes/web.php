<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\MuxEmailController;
use App\Http\Controllers\ShopifyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return view('dashboard');
})->middleware(['verify.shopify'])->name('home');

Route::get('flush', function () {
    request()->session()->flush();
});

Route::group(['middleware' => ['verify.shopify']], function () {

    Route::prefix('test')->group(function () {
        Route::get('/', [TestController::class, 'index']); //test-api deafult use in vue
        Route::get('/api', [TestController::class, 'TestAPIData']); //test-api deafult use in vue
        Route::get('/mux-email/connect', [TestController::class, 'connectMuxemailTest']); //test-api deafult use in vue
        Route::get('/webhook-list', [TestController::class, 'RegisteredwebhooksList']); //
        Route::get('/create/webhook', [TestController::class, 'RegisterWebhooksTest']); //
        Route::get('/delete/mux-customer', [TestController::class, 'deleteMuxEmailContact']); //
        Route::get('/webhook/customer/delete', [TestController::class, 'deleteCustomerWebhook']); //
    });

    //MUX-EMAIL API--------------------------
    Route::prefix('mux-email')->group(function () {
        Route::post('/connect', [MuxEmailController::class, 'connectMuxEmail']);
        Route::get('/get-attributes-fields', [MuxEmailController::class, 'getAttributesFields']);
        Route::get('/get-all-tags', [MuxEmailController::class, 'getAllTags']);
        Route::post('/get-all-workspaces', [MuxEmailController::class, 'getAllWorkspaces']);
        Route::get('/check-credential', [MuxEmailController::class, 'CheckCredential']);
    });

    Route::get('/get-customers-count', [ShopifyController::class, 'getCustomersCount']);
    Route::get('/get-customer-fields', [ShopifyController::class, 'getCustomerFields']);
    Route::get('/get-bulk-contacts', [ShopifyController::class, 'getBulkContacts']);
    Route::post('/bulk-upload-contacts', [ShopifyController::class, 'BulkUploadSyncContacts']);
    Route::post('/add-attributes-fields', [AppController::class, 'AddAttributesFields']);
    Route::get('/get-match-attributes-fields', [AppController::class, 'GetMatchAttributesFields']);
    Route::post('/save-tags', [AppController::class, 'SaveTags']);
    Route::get('/get-assign-tags', [AppController::class, 'GetAssignTags']);


});
